from django.urls import path
from django.contrib.auth import views as auth_views

from . import views


app_name = "accounts"
urlpatterns = [
    path('login/', auth_views.LoginView.as_view(), name="login"),

    path('register/', views.UserCreateView.as_view(), name='register'),
    # path('register/', views.register, name='register'),
    
    path('group-create/', views.GroupCreateView.as_view(), name='group_create'),
    path('users/', views.UserListView.as_view(), name='users_list'),
    path('groups/', views.GroupListView.as_view(), name='groups_list'),
]