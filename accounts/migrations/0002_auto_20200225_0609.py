# Generated by Django 3.0.2 on 2020-02-25 06:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='user',
            options={'permissions': (('can_delete_other_users', 'Can delete other users'), ('can_rear_pokemon', 'Able to rear pokemon'))},
        ),
    ]
