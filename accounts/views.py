from django.views import View 
from django.urls import reverse_lazy
from django.shortcuts import render,redirect
from django.contrib.auth import authenticate , get_user_model , views as auth_views
from django.contrib.auth.models import Group
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.views.generic.edit import CreateView
from django.views.generic import ListView, DetailView

from .forms import CustomUserCreationForm

User = get_user_model()

def login(request, **kwargs):
    pass
  
# Registering user with function based views.
def register(request):
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Account created successfully')
            return redirect('accounts:register')
    else:
        form = CustomUserCreationForm()
    return render(request, 'accounts/register.html', {'form': form})
    
# Registering user with class based views. 
class UserCreateView(CreateView):
    template_name = "accounts/register.html"
    model = User
    fields = ['username','email','groups','user_permissions']
    success_url = reverse_lazy("accounts:users_list")

class GroupCreateView(CreateView):
    template_name = "accounts/group_create.html"
    model = Group
    fields = "__all__"
    success_url = reverse_lazy("accounts:groups_list")
    
class GroupListView(ListView):
    template_name = "accounts/groups_list.html"
    model = Group
    context_object_name = "Groups"

class UserListView(ListView):
    model = User
    template_name = 'accounts/users_list.html'
    context_object_name = 'Users'