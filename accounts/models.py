from django.db import models
from django.contrib.auth.models import AbstractUser 



## Defining models 

class User(AbstractUser):
    email       = models.EmailField(unique=True)
    nickname    = models.CharField(max_length=50, null=True, blank=True)

    class Meta:
        permissions = (
            ("can_delete_other_users","Can delete other users"),
            ("can_rear_pokemon","Able to rear pokemon"),
        )

class Profile(models.Model):
    user        = models.OneToOneField(User, on_delete=models.CASCADE)
    bio         = models.TextField(max_length=500, blank=True)
    address     = models.CharField("Permanent Address", max_length=100)
    birth_date  = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    company     = models.CharField(max_length=100, blank=True, null=True)


